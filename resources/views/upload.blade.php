<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta property="og:title" content="Link Upload Foto Bukti" />
    <meta property="og:description" content="Upload Bukti Pengaduanmu" />
    {{-- <meta property="og:url"
        content="https://solu.co.id/pesat/" /> --}}
    <meta property="og:image"
        content="https://solu.co.id/pesat/logo1.jpg" />
    <title>Upload Bukti Pengaduan</title>

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
    <link href="{{ secure_asset('css/style.css') }}" rel="stylesheet">


    <script type="text/javascript" src="//code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

</head>
<body>

<div class="container mt-5">

    <div class="card">
        <!-- For demo purpose -->
        <div class="row text-center text-grey mb-3">
            <div class="col-lg-7 mx-auto">
            <h1 class="mt-4">Upload Bukti Pengaduan</h1>
            <p class="text-center">Upload bukti pengaduan kamu disini!</p>
            </div>
        </div>
        <!-- End -->

        <div class="row">   
        <div class="col-lg-8 mx-auto">
                <form method="post" enctype="multipart/form-data" action="{{ route('uploadBukti') }}">
                    {{csrf_field()}}
                    <div class="card-body">
                        <div class="form-group">
                            <label>Complaint No</label>
                            <input type="text" class="form-control" name="complaint_no" value="{{ $TComplaint->complaint_no }}" readonly>
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            <input type="text" class="form-control" name="description" value="{{ $TComplaint->description }}" readonly>
                        </div>
                        @if(Session::has('message'))
                            <div class="alert alert-danger">
                                {{Session::get('message')}}
                            </div>
                        @endif
                        <div class="form-group">
                            <label>Upload Pengaduan</label>
                            <input type="file" class="form-control" name="picture" required>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        <button class="btn btn-primary mr-1" type="submit">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<!-- TODO: Missing CoffeeScript 2 -->

<script type="text/javascript">//<![CDATA[//]]></script>
<script>
  // tell the embed parent frame the height of the content
  if (window.parent && window.parent.parent){
    window.parent.parent.postMessage(["resultsFrame", {
      height: document.body.getBoundingClientRect().height,
      slug: "dh9bt7up"
    }], "*")
  }

  // always overwrite window.name, in case users try to set it manually
  window.name = "result"
</script>
</body>
</html>