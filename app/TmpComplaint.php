<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TmpComplaint extends Model
{
    // define table
    protected $table = 'tmp_complaint';
    protected $primaryKey = 'id';

    protected $guarded = [];

}
