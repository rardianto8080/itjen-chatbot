<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class m_complaint extends Model
{
    // define table
    protected $table = 'm_complaints';
    protected $primaryKey = 'id';

    protected $guarded = [];

}
