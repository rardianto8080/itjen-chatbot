<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    // define table
    protected $table = 'users';
    protected $primaryKey = 'id';

    protected $guarded = [];

}
