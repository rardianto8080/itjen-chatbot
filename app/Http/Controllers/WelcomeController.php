<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DateTime;

class WelcomeController extends Controller
{
    function greeting($phone)
    {
        $dt    = new DateTime();
        $jam   = $dt->format('H')+7;

        if ($jam < 12)
        {
            $greet = "Selamat Pagi!";
        }
        else if ($jam < 15)
        {
            $greet = "Selamat Siang";
        }
        else if ($jam < 18)
        {
            $greet = "Selamat Sore";
        }
        else
        {
            $greet = "Selamat Malam";
        }

        $user = \DB::table('users')->where('telp_no', $phone)->first();

        if (!is_null($user)){
            $response = "Haloo $greet, $user->name";
            $response .= "\n\nSelamat datang di posko pengaduan Inspektorat Jenderal Kementerian Pendidikan, Kebudayaan, Riset, dan Teknologi";
            $response  .= "\n1. Registrasi Pelapor\n";
            $response .= "2. Ajukan Pengaduan\n";
            $response .= "3. Tracking Pengaduan\n";
            $response .= "4. Hapus Pengaduan\n";
            $response .= "5. Info";
    
            return $response;
         } else {
            $response = "Haloo $greet";
            $response .= "\n\nSelamat datang di posko pengaduan Inspektorat Jenderal Kementerian Pendidikan, Kebudayaan, Riset, dan Teknologi";
            $response  .= "\n1. Registrasi Pelapor\n";
            $response .= "2. Ajukan Pengaduan\n";
            $response .= "3. Tracking Pengaduan\n";
            $response .= "4. Hapus Pengaduan\n";
            $response .= "5. Info";

            return $response;
        }
    }
}