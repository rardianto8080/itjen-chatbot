<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Users;
use App\TmpUsers;
use App\TComplaint;
use App\TmpComplaint;
use Carbon\Carbon;
use App\Twilio;

class HomeController extends Controller
{
    
    public function index()
    {
        $request       = file_get_contents("php://input");
        $requestJson   = json_decode($request, true);
        $action        = $requestJson['queryResult']['action'];

        $phoneRequest  = $requestJson['originalDetectIntentRequest']['payload']['From'];
        $phone         = str_replace("whatsapp:","",$phoneRequest);
        
        
        // $date = Carbon::parse('02/10/2020 00:00:00');
        // $customer   = Users::where('telp_no', $phone);
        //     TComplaint::where([
        //             ['m_user_fe_id',14]
        //         ])->orderBy('created_at','DESC')->first()->update([
        //         'date' =>  $date
        //     ]);
        // return $this->fulfillment('tes');

        // $name       = (isset($requestJson["queryResult"]["queryText"]) ? $requestJson["queryResult"]["queryText"] : null);
        // $customer   = Users::where('phone', $phone);
        // $customer->create([
        //     'name' => 'tes2',
        //     'email' => 'tes',
        //     'telp_no' => '123123',
        //     'is_active' => 1,
        //     'device_id' => '11232321312',
        //     'created_by' => '1',
        //     'password' => '$2y$10$Qk4hwV8eChBn8biBSu0xVOvV5Poxp0BYHgSMUP65UbKBi7JmKoRP6',
        //     'image' => 1
        // ]);

        
        if($action == 'fallback_session') {
            $customer   = Users::where('telp_no', $phone)->first();
            if($customer->session == "home" || $customer->session == ""){
                $response = "Mohon isi sesuai format.\n\n";
                $response .= "1. Registrasi Pelapor\n";
                $response .= "2. Ajukan Pengaduan\n";
                $response .= "3. Tracking Pengaduan\n";
                $response .= "4. Hapus Pengaduan\n";
                $response .= "5. Info";
            }else if($customer->session == "hapus_akun"){
                $response = "Mohon isi sesuai format.\n\n";
                $response .= "1. Menu\n";
                $response .= "2. Edit Data\n";
                $response .= "3. Hapus Akun Pelapor";
            }else if($customer->session == "variable_akun"){
                $response = "Mohon isi sesuai format.\n\n";
                $response .= "1. Nama\n";
                $response .= "2. Email\n";
                $response .= "3. NIK\n";
                $response .= "4. SIM\n";
                $response .= "5. Kartu Pelajar\n";
                $response .= "6. Kartu Mahasiswa\n";
                $response .= "7. Kembali";
            }else if($customer->session == "yb"){
                $response = "Mohon isi sesuai format.\n\n";
                $response .= "1. Ya\n";
                $response .= "2. Tidak\n";
            }else if($customer->session == "yk"){
                $response = "Mohon isi sesuai format.\n\n";
                $response .= "1. Ya\n";
                $response .= "2. Kembali\n";
            }else if($customer->session == "yb_np"){
                $response = "Mohon isi sesuai format.\n\n";
                $response .= "1. Menu\n";
                $response .= "2. Cari nomor pengaduan\n";   
            }else if($customer->session == "yn"){
                $response = "Mohon isi sesuai format.\n\n";
                $response .= "1. Ya\n";
                $response .= "2. Belum\n";
            }else if($customer->session == "ynb"){
                $response = "Mohon isi sesuai format.\n\n";
                $response .= "1. Ya\n";
                $response .= "2. Tidak\n";
                $response .= "3. Kembali\n";
            }else if($customer->session == "isidata_akun"){
                $response = "Mohon isi sesuai format.\n\n";
                $response .= "1. Ya\n";
                $response .= "2. Batal menyimpan data\n";
                $response .= "3. Lanjut isi data";
            }else if($customer->session == "complaint"){
                $response = "Mohon isi sesuai format.\n\n";
                $response .= "1. Nama Terlapor\n";
                $response .= "2. Tanggal Kejadian\n";
                $response .= "3. Lokasi Kejadian\n";
                $response .= "4. Deskripsi Pengaduan\n";
                $response .= "5. Bukti Pengaduan\n";
                $response .= "6. Clue Lain\n";
                $response .= "7. Kembali";
            }else if($customer->session == "track"){
                $response = "Mohon isi sesuai format\n\n";
                $response .= "1. Menu\n";
                $response .= "2. Tracking";
            }
            else if($customer->session == "variable_type_complaint"){
                $response = "Mohon isi sesuai format\n\n";
                $id = 1;
                foreach($m_complaint as $complaint => $value){
                    $response   .= $value->id . ". $value->name\n";    
                    $id = $value->id;
                }
            }
            else if($customer->session == "menu_doang"){
                $response = "Mohon isi sesuai format.\n\n";
                $response .= "1. Menu";
            }

            return $this->fulfillment($response);
        }

        if($action=="input_profile"){
            $profile_text = (isset($requestJson["queryResult"]["queryText"]) ? $requestJson["queryResult"]["queryText"] : null);
            $customer   = Users::where('telp_no', $phone);
            $checker    = $customer->first();

            if (($pos1 = strpos($profile_text, "\nNama : ")) !== false) { 
                $pos1 = strpos($profile_text,":");
                $name = substr($profile_text, $pos1+2); 
                $name = substr($name, 0, strpos($name, "\n"));
    
                if (($pos2 = strpos($profile_text, "\nEmail : ")) !== FALSE) { 
                    if ((($pos2 = strpos($profile_text, "@")) !== FALSE) || (($pos2 = strpos($profile_text, ".")) !== FALSE)) { 
                        $pos2 = strpos($profile_text,":",52);
                        $email = substr($profile_text, $pos2+2); 
                        if((strpos($email, "\n")) !== FALSE){
                            $email = substr($email, 0, strpos($email, "\n"));
                        }else{
                            $email = substr($profile_text, $pos2+2);
                        }
                        
                        // jika customer belum terdaftar
                        if (is_null($checker)) {
                            $customer->create([
                                'name' => $name,
                                'no_telp' => $phone,
                                'email' => $email
                            ]);
                        } else {
                            // jika customer sudah terdaftar
                            $customer->update([
                                'name' => $name,
                                'email' => $email
                            ]);
                        }

                        $customer   = Users::where('telp_no', $phone)->first();
                        $response = "Terima kasih sudah melakukan registrasi.
                        \n\nProfil yang terdaftar :
                        \nNama : $customer->name
                        \nEmail : $customer->email";
                    }else{
                        $response = "Mohon isi sesuai format. (Email)\nContoh : example0980@gmail.com\n\n1. Menu";
                    }
                }else{
                    $response = "Mohon isi sesuai format. (Email)\n\n1. Menu";
                }
            }else{
                $response = "Mohon isi sesuai format. (Nama)\n\n1. Menu";
            }

            Users::where('telp_no', $phone)->update([
                        'session' => 'menu_doang'
                    ]);

            return $this->fulfillment_withContext($response, "projects/chatbot-itjen/agent/sessions/whatsapp:$phone/contexts/1Regis-followup");
        }

        if($action=="menu_doang"){
            Users::where('telp_no', $phone)->update([
                'session' => 'menu_doang'
            ]);

            return $response;
        }

        if($action=="input_aduan"){
            $complaint_text = (isset($requestJson["queryResult"]["queryText"]) ? $requestJson["queryResult"]["queryText"] : null);
            $customer   = Users::where('telp_no', $phone);
            $checker    = $customer->first();

            if (($pos1 = strpos($complaint_text, "\nNama Terlapor : ")) !== false) { 
                $pos1 = strpos($complaint_text,":");
                $name = substr($complaint_text, $pos1+2); 
                $name = substr($name, 0, strpos($name, "\n"));
                $update_complaint_text = substr($complaint_text, $pos1+2); 
    
                if (($pos2 = strpos($update_complaint_text, "\nTanggal Kejadian (dd/mm/yyyy) : ")) !== FALSE) { 
                    $pos1 = strpos($update_complaint_text,":");
                    $date = substr($update_complaint_text, $pos1+2); 
                    $original_date = substr($date, 0, strpos($date, "\n"));
                    try{
                        $date = Carbon::createFromFormat('d/m/Y', $original_date)->format('Y/m/d');
                    }catch(\Exception $e){
                        $response = "Mohon isi sesuai format tanggal (tanggal/bulan/tahun). (Tanggal Kejadian)\n\n1. Menu";
                        Users::where('telp_no', $phone)->update([
                            'session' => 'menu_doang'
                        ]);
    
                        return $this->fulfillment_withContext($response, "projects/chatbot-itjen/agent/sessions/whatsapp:$phone/contexts/2BuatAduan-followup");
                    }
                    // return $this->fulfillment($date);
                    $update_complaint_text = substr($update_complaint_text, $pos1+2); 
        
                    if (($pos2 = strpos($update_complaint_text, "\nLokasi Kejadian : ")) !== FALSE) { 
                        $pos1 = strpos($update_complaint_text,":");
                        $location = substr($update_complaint_text, $pos1+2); 
                        $location = substr($location, 0, strpos($location, "\n"));
                        $update_complaint_text = substr($update_complaint_text, $pos1+2); 
            
                        if (($pos2 = strpos($update_complaint_text, "\nDeskripsi : ")) !== FALSE) { 
                            $pos1 = strpos($update_complaint_text,":");
                            $desc = substr($update_complaint_text, $pos1+2); 
                            $desc = substr($desc, 0, strpos($desc, "\n"));
                            $update_complaint_text = substr($update_complaint_text, $pos1+2); 
                
                            if (($pos2 = strpos($update_complaint_text, "\nClue Tambahan : ")) !== FALSE) { 
                                $pos1 = strpos($update_complaint_text,":");
                                $clue = substr($update_complaint_text, $pos1+2); 
                                $clue = substr($clue, 0, strpos($clue, "\n"));
                                $update_complaint_text = substr($update_complaint_text, $pos1+2); 
                    
                            // jika customer belum terdaftar
                            if (is_null($checker)) {
                                $response = "Data Anda belum terdaftar, silakan daftarkan diri Anda melalui menu Registrasi Pelapor\n\n1. Menu";
                                return $this->fulfillment_withContext($response, "projects/chatbot-itjen/agent/sessions/whatsapp:$phone/contexts/InputAduan-1Kirim-followup");
                            } else {
                                // jika customer sudah terdaftar
                                // TmpComplaint::create([
                                //         'm_user_fe_id' => $customer->first()->id,
                                //         'nama_terlapor' => $name,
                                //         'original_date' => $original_date,
                                //         'date' => $date,
                                //         'location' => $location,
                                //         'description' => $desc,
                                //         'clue' => $clue
                                //     ]);
                                
                                $complaint_last = \DB::table('t_complaint')->where([
                                    ['complaint_no', 'LIKE', 'C%']
                                    ])->orderBy('id','DESC')->first();
                                
                                $complaint_no = ++$complaint_last->complaint_no;


                                TComplaint::create([
                                    'm_user_fe_id' => $customer->first()->id,
                                    'complaint_no' => $complaint_no,
                                    'created_by' => $customer->first()->id,
                                    'updated_by' => $customer->first()->id,
                                    'nama_terlapor' => $name,
                                    'date' => $date,
                                    'location' => $location,
                                    'description' => $desc,
                                    'clue' => $clue
                                ]);
                            }
                            $response = "Silakan upload bukti pengaduan Anda (jika ada) melalui link berikut.\n";
                            $response .= "https://itjen-chatbot.herokuapp.com/upload/". $complaint_no;
                            $response .= "\n\n1. Kirim Aduan\n2. Kembali\n3. Menu";
                            // $complaint   = TComplaint::where('complaint_no', $complaint_no)->first();
                            // $response = "Terima kasih sudah membuat aduan.
                            // \n\nDengan detail aduan sebagai berikut :
                            // \nNama Terlapor : $complaint->nama_terlapor
                            // \nTanggal Kejadian : $original_date
                            // \nLokasi Kejadian : $complaint->location
                            // \nDeskripsi : $complaint->description
                            // \nClue tambahan : $complaint->clue";
                            }else{
                                $response = "Mohon isi sesuai format. (Clue Tambahan)\n\n1. Menu";
                            }
                        }else{
                            $response = "Mohon isi sesuai format. (Deskripsi)\n\n1. Menu";
                        }
                    }else{
                        $response = "Mohon isi sesuai format. (Lokasi Kejadian)\n\n1. Menu";
                    }
                }else{
                    $response = "Mohon isi sesuai format. (Tanggal Kejadian)\n\n1. Menu";
                }
            }else{
                $response = "Mohon isi sesuai format. (Nama Terlapor)\n\n1. Menu";
            }

            Users::where('telp_no', $phone)->update([
                        'session' => 'menu_doang'
                    ]);

            return $this->fulfillment_withContext($response, "projects/chatbot-itjen/agent/sessions/whatsapp:$phone/contexts/2BuatAduan-followup");
        }

        // //hapus nomor aduan
        // if($action == 'menu_doang') {
        //     $response = "Silahkan masukkan no pengaduan yang ingin dihapus :\n\n";
        //     $response .= "1. Menu\n";
                
        //     Users::where('telp_no', $phone)->update([
        //         'session' => 'menu_doang'
        //     ]);

        //     return $this->fulfillment($response);
        // }

        // //lacak nomor aduan
        // if($action == 'menu_doang2') {
        //     $response = "Silahkan masukkan no pengaduan Anda :\n\n";
        //     $response .= "1. Menu\n";
                
        //     Users::where('telp_no', $phone)->update([
        //         'session' => 'menu_doang'
        //     ]);

        //     return $this->fulfillment($response);
        // }

        // if($action == 'RegistrasiPelapor_cek') {
        //     $customer   = Users::where('telp_no', $phone);
        //     $checker    = $customer->first();
        //     // jika customer belum terdaftar
        //     if (is_null($checker)) {
        //         $response = "Anda belum terdaftar.\n";
        //         $response .= "Nama : \n";
        //         $response .= "Email : \n";
        //         $response .= "Tanggal Registrasi : \n";
        //         $response .= "NIK : \n";
        //         $response .= "Kartu Pelajar : \n";
        //         $response .= "Kartu Mahasiswa :\n\n";
        //         $response .= "1. Menu\n";
        //         $response .= "2. Isi Data\n";
        //         // $response="Pilih data yang akan diisi.\n";
        //         // $response="1. Nama\n";
        //         // $response="2. Email\n";
        //         // $response="3. NIK\n";
        //         // $response="4. SIM\n";
        //         // $response="5. Kartu Pelajar\n";
        //         // $response="6. Kartu Mahasiswa\n";
        //         // $response="7. Kembali\n";
        //     } else {
        //         $date = date('d-m-Y', strtotime($customer->first()->created_at));

        //         $response = "Pelapor sudah terdaftar.\n";
        //         $response .= "Nama : ". $customer->first()->name ."\n";
        //         $response .= "Email : ". $customer->first()->email ."\n";
        //         $response .= "Tanggal Registrasi : ". $date ."\n";
        //         $response .= "NIK : ". $customer->first()->NIK ."\n";
        //         $response .= "Kartu Pelajar : ". $customer->first()->KartuPelajar ."\n";
        //         $response .= "Kartu Mahasiswa : ". $customer->first()->KartuMahasiswa ."\n\n";
        //         $response .= "1. Menu\n";
        //         $response .= "2. Edit Data\n";
        //         $response .= "3. Hapus Akun Pelapor\n";
        //     }
        //     Users::where('telp_no', $phone)->update([
        //         'session' => 'hapus_akun'
        //     ]);

        //     return $this->fulfillment($response);
        // }
        
        // if($action == 'confirm_hapus_akun') {
        //     $response = "Apakah Anda yakin ingin menghapus akun pelapor Anda?\n\n";
        //     $response .= "1. Ya\n";
        //     $response .= "2. Tidak";

        //     Users::where('telp_no', $phone)->update([
        //         'session' => 'yb'
        //     ]);

        //     return $this->fulfillment($response);
        // }

        // if($action == 'HapusAkun_Notif') {
        //     $confirm       = (isset($requestJson["queryResult"]["queryText"]) ? $requestJson["queryResult"]["queryText"] : null);
        //     if($confirm == 1){
        //         $customer   = Users::where('telp_no', $phone)->delete();
        //         $response = "Data Anda berhasil dihapus.\n\n1. Menu";
        //     }else if($confirm == 2){
        //         $response = "Data Anda tidak jadi dihapus.\n\n1. Menu";
        //     }
        //     Users::where('telp_no', $phone)->update([
        //         'session' => 'home'
        //     ]);
        //     return $this->fulfillment($response);
        // }
        
        // if($action == 'InfoProfilPelapor_Ubah') {
        //     $customer = Users::where('telp_no', $phone)->first();
        //     if(!is_null($customer)){
        //         $regis = Carbon::createFromFormat('Y-m-d H:i:s', $customer->created_at)->format('d-m-Y');
        //     }else{
        //         $regis = "-";
        //     }

        //     $response = "Berikut data profil Anda.\n";
        //     $response .= "Nama : ". $customer->name ."\n";
        //     $response .= "Email : ". $customer->email ."\n";
        //     $response .= "Tanggal Registrasi : ". $regis ."\n";
        //     $response .= "NIK : ". $customer->NIK ."\n";
        //     $response .= "SIM : ". $customer->SIM ."\n";
        //     $response .= "Kartu Pelajar : ". $customer->KartuPelajar ."\n";
        //     $response .= "Kartu Mahasiswa : ". $customer->KartuMahasiswa ."\n\n";
        //     $response .= "Pilih data yang akan diisi.\n";
        //     $response .= "1. Nama\n";
        //     $response .= "2. Email\n";
        //     $response .= "3. NIK\n";
        //     $response .= "4. SIM\n";
        //     $response .= "5. Kartu Pelajar\n";
        //     $response .= "6. Kartu Mahasiswa\n";
        //     $response .= "7. Kembali";

        //     Users::where('telp_no', $phone)->update([
        //         'session' => 'variable_akun'
        //     ]);

        //     return $this->fulfillment($response);
        // }

        // if($action == 'isiData_InputVariable') {
        //     $input = (isset($requestJson["queryResult"]["queryText"]) ? $requestJson["queryResult"]["queryText"] : null);

        //     $customer   = Users::where('telp_no', $phone)->first();
        //     $tmp_customer   = TmpUsers::where('telp_no', $phone)->first();

        //     if($input == 1){
        //         $response = "Silakan ketikkan nama Anda";
        //         $var = '1';
        //     }else if($input == 2){
        //         $response = "Silakan ketikkan email Anda";
        //         $var = '2';
        //     }else if($input == 3){
        //         $response = "Silakan ketikkan NIK Anda";
        //         $var = '3';
        //     }else if($input == 4){
        //         $response = "Silakan ketikkan no. SIM Anda";
        //         $var = '4';
        //     }else if($input == 5){
        //         $response = "Silakan ketikkan no. Kartu Pelajar Anda";
        //         $var = '5';
        //     }else if($input == 6){
        //         $response = "Silakan ketikkan no. Kartu Mahasiswa Anda";
        //         $var = '6';
        //     }
            
        //     if(is_null($tmp_customer)){
        //         if(!is_null($customer)){
        //             \DB::table('tmp_users')->insert([
        //                 'name' => $customer->name,
        //                 'NIK' => $customer->NIK,
        //                 'email' => $customer->email,
        //                 'telp_no' => $customer->telp_no,
        //                 'SIM' => $customer->SIM,
        //                 'updated_by' => $customer->id,
        //                 'updated_at' => Carbon::now(),
        //                 'tmp_variable' => $var
        //             ]);    
        //         }else{
        //             \DB::table('tmp_users')->insert([
        //                 'name' => '',
        //                 'telp_no' => $phone,
        //                 'updated_at' => Carbon::now(),
        //                 'tmp_variable' => $var
        //             ]);    
        //         }
        //     }else{
        //         $tmp_customer   = TmpUsers::where('telp_no', $phone);
        //         $tmp_customer->update([
        //             'tmp_variable' => $var
        //         ]);
        //     }
            
        //     return $this->fulfillment($response);
        // }

        // InputVariable.Confirm User
        // if($action == 'InputVariable.Confirm') {
        //     $var = (isset($requestJson["queryResult"]["queryText"]) ? $requestJson["queryResult"]["queryText"] : null);
        //     $tmp_customer   = TmpUsers::where('telp_no', $phone);
        //     $tmp_customer->update([
        //         'tmp_value' => $var
        //     ]);

        //     if(($tmp_customer->first()->tmp_variable) == 1){
        //         $tmp_variable = "nama";
        //     }else if(($tmp_customer->first()->tmp_variable) == 2){
        //         $tmp_variable = "email";
        //     }else if(($tmp_customer->first()->tmp_variable) == 3){
        //         $tmp_variable = "NIK";
        //     }else if(($tmp_customer->first()->tmp_variable) == 4){
        //         $tmp_variable = "SIM";
        //     }else if(($tmp_customer->first()->tmp_variable) == 5){
        //         $tmp_variable = "Kartu Pelajar";
        //     }else if(($tmp_customer->first()->tmp_variable) == 6){
        //         $tmp_variable = "Kartu Mahasiswa";
        //     }

        //     $response = "Apakah '". $var ."' sudah benar sebagai ". $tmp_variable ." ?\n\n";
        //     $response .= "1. Ya\n";
        //     $response .= "2. Kembali\n";

        //     Users::where('telp_no', $phone)->update([
        //         'session' => 'yk'
        //     ]);

        //     return $this->fulfillment($response);
        // }

        // InputVariable.Confirm Complaint
        // if($action == 'InputVariableComplaint.Confirm') {
        //     $var = (isset($requestJson["queryResult"]["queryText"]) ? $requestJson["queryResult"]["queryText"] : null);
        //     $customer  = Users::where('telp_no', $phone);
        //     $tmp_complaint   = TComplaint::where([
        //         ['m_user_fe_id',$customer->first()->id]
        //         ])->whereNotNull('tmp_variable')->orderBy('created_at','DESC');
            
        //     if(($tmp_complaint->first()->tmp_variable) != 5){
        //         if(($tmp_complaint->first()->tmp_variable) == 1){
        //             $tmp_variable = "nama terlapor";
        //         }else if(($tmp_complaint->first()->tmp_variable) == 2){
        //             $tmp_variable = "tanggal kejadian";
        //         }else if(($tmp_complaint->first()->tmp_variable) == 3){
        //             $tmp_variable = "lokasi kejadian";
        //         }else if(($tmp_complaint->first()->tmp_variable) == 4){
        //             $tmp_variable = "deksripsi pengaduan";
        //         }else if(($tmp_complaint->first()->tmp_variable) == 6){
        //             $tmp_variable = "clue lain";
        //         }
        //         $response = "Apakah '". $var ."' sudah benar sebagai ". $tmp_variable ." ?\n\n";
        //         $response .= "1. Ya\n";
        //         $response .= "2. Kembali\n";
                
        //         $TComplaint = TComplaint::where([
        //             ['m_user_fe_id',$customer->first()->id]
        //             ])->orderBy('created_at','DESC')->first();

        //         $TComplaint->update([
        //             'tmp_value' => $var
        //         ]);
        //     }

        //     Users::where('telp_no', $phone)->update([
        //         'session' => 'yk'
        //     ]);
        
        //     return $this->fulfillment($response);
        // }
        
        //konfirmasi menyimpan data user
        // if($action == 'konfirmasi_menyimpan_data') {
        //     $tmp_customer  = TmpUsers::where('telp_no', $phone);
        //     if(($tmp_customer->first()->tmp_variable) == 1){
        //         $tmp_customer->update([
        //             'name' => $tmp_customer->first()->tmp_value
        //         ]);
        //     }else if(($tmp_customer->first()->tmp_variable) == 2){
        //         $tmp_customer->update([
        //             'email' => $tmp_customer->first()->tmp_value
        //         ]);
        //     }else if(($tmp_customer->first()->tmp_variable) == 3){
        //         $tmp_customer->update([
        //             'NIK' => $tmp_customer->first()->tmp_value
        //         ]);
        //     }else if(($tmp_customer->first()->tmp_variable) == 4){
        //         $tmp_customer->update([
        //             'SIM' => $tmp_customer->first()->tmp_value
        //         ]);
        //     }else if(($tmp_customer->first()->tmp_variable) == 5){
        //         $tmp_customer->update([
        //             'KartuPelajar' => $tmp_customer->first()->tmp_value
        //         ]);
        //     }else if(($tmp_customer->first()->tmp_variable) == 6){
        //         $tmp_customer->update([
        //             'KartuMahasiswa' => $tmp_customer->first()->tmp_value
        //         ]);
        //     }
        //     $tmp_customer  = TmpUsers::where('telp_no', $phone)->first();
        //     $customer   = Users::where('telp_no', $phone)->first();
        //     if(!is_null($customer)){
        //         $regis = Carbon::createFromFormat('Y-m-d H:i:s', $customer->created_at)->format('d-m-Y');
        //     }else{
        //         $regis = "-";
        //     }
            
        //     $response = "Berikut data profil Anda.\n";
        //     $response .= "Nama : ". $tmp_customer->name ."\n";
        //     $response .= "Email : ". $tmp_customer->email ."\n";
        //     $response .= "Tanggal Registrasi : ". $regis ."\n";
        //     $response .= "NIK : ". $tmp_customer->NIK ."\n";
        //     $response .= "SIM : ". $tmp_customer->SIM ."\n";
        //     $response .= "Kartu Pelajar : ". $tmp_customer->KartuPelajar ."\n";
        //     $response .= "Kartu Mahasiswa : ". $tmp_customer->KartuMahasiswa ."\n\n";
        //     $response .= "Apakah Anda sudah selesai dengan perubahan data Anda?.\n";
        //     $response .= "1. Ya.\n";
        //     $response .= "2. Batal menyimpan data.\n";
        //     $response .= "3. Lanjut isi data.";

        //     Users::where('telp_no', $phone)->update([
        //         'session' => 'isidata_akun'
        //     ]);
            
        //     return $this->fulfillment($response);
        // }

        //input to db data users
        // if($action == 'InputText_SaveToDB') {
        //     $customer   = Users::where('telp_no', $phone)->first();
        //     $tmp_customer   = TmpUsers::where('telp_no', $phone)->first();

        //     if(!is_null($customer)){
        //         $customer->update([
        //             'name'      => $tmp_customer->name,
        //             'NIK'       => $tmp_customer->NIK,
        //             'email'     => $tmp_customer->email,
        //             'telp_no'   => $tmp_customer->telp_no,
        //             'SIM'       => $tmp_customer->SIM,
        //             'KartuPelajar'       => $tmp_customer->KartuPelajar,
        //             'KartuMahasiswa'       => $tmp_customer->KartuMahasiswa,
        //             'updated_by' => $tmp_customer->id,
        //             'updated_at' => $tmp_customer->updated_at
        //         ]);
        //     }else{
        //         Users::insert([
        //             'name'      => $tmp_customer->name,
        //             'NIK'       => $tmp_customer->NIK,
        //             'email'     => $tmp_customer->email,
        //             'telp_no'   => $tmp_customer->telp_no,
        //             'SIM'       => $tmp_customer->SIM,
        //             'KartuPelajar'       => $tmp_customer->KartuPelajar,
        //             'KartuMahasiswa'       => $tmp_customer->KartuMahasiswa,
        //             'created_by' => $tmp_customer->id,
        //             'created_at' => Carbon::now()
        //         ]);
        //         $customer   = Users::where('telp_no', $phone)->first();
        //         Users::where('telp_no', $phone)->first()->update([
        //             'created_by' => $customer->id
        //         ]);
        //     }
            
        //     TmpUsers::where('telp_no', $phone)->delete();
        //     $customer   = Users::where('telp_no', $phone)->first();
        //     $regis = Carbon::createFromFormat('Y-m-d H:i:s', $customer->created_at)->format('d-m-Y');

        //     $response = "Data Anda berhasil disimpan.\n";
        //     $response .= "Nama : ". $customer->name ."\n";
        //     $response .= "Email : ". $customer->email ."\n";
        //     $response .= "Tanggal Registrasi : ". $regis ."\n";
        //     $response .= "NIK : ". $customer->NIK ."\n";
        //     $response .= "SIM : ". $customer->SIM ."\n";
        //     $response .= "Kartu Pelajar : ". $customer->KartuPelajar ."\n";
        //     $response .= "Kartu Mahasiswa : ". $customer->KartuMahasiswa ."\n\n";
        //     $response .= "1. Menu";

        //     Users::where('telp_no', $phone)->update([
        //         'session' => 'home'
        //     ]);

        //     return $this->fulfillment($response);
        // }

        // if($action == 'AjukanPengaduan_InputVariable') {
        //     $input = (isset($requestJson["queryResult"]["queryText"]) ? $requestJson["queryResult"]["queryText"] : null);

        //     $customer   = Users::where('telp_no', $phone);
        //     $TComplaint = TComplaint::where([
        //             ['m_user_fe_id',$customer->first()->id]
        //             ])->orderBy('created_at','DESC')->first();

        //     if($input == 1){
        //         $response = "Silakan ketikkan nama terlapor.";
        //         $var = '1';
        //     }else if($input == 2){
        //         $response = "Silakan masukkan tanggal kejadian dengan format sebagai berikut (tanggal/bulan/tahun). Contoh 25/08/2021";
        //         $var = '2';
        //     }else if($input == 3){
        //         $response = "Silakan masukkan lokasi kejadian. (Contoh: Kompleks Kemendikbud Gd B, Jl. Jend. Sudirman, RT.1/RW.3, Senayan, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10270)";
        //         $var = '3';
        //     }else if($input == 4){
        //         $response = "Silakan masukkan deskripsi pengaduan Anda.";
        //         $var = '4';
        //     }else if($input == 5){
        //         $response = "Silakan upload bukti pengaduan Anda melalui link berikut.\n";
        //         $response .= "https://itjen-chatbot.herokuapp.com/upload/". $TComplaint->complaint_no;
        //         $response .= "\n\n1. Kembali";
                
        //         $var = '5';
        //     }else if($input == 6){
        //         $response = "Silakan masukkan clue tambahan dari pengaduan Anda.";
        //         $var = '6';
        //     }
            
        //     TComplaint::where([
        //         ['m_user_fe_id',$customer->first()->id]
        //         ])->orderBy('created_at','DESC')->first()->update([
        //         'tmp_variable' => $var
        //     ]);

        //     return $this->fulfillment($response);
        // }

        if($action == 'input.welcome') {
            // $response = "tes";
            $response = app('App\Http\Controllers\WelcomeController')->greeting($phone);
            Users::where('telp_no', $phone)->update([
                        'session' => 'home'
                    ]);
            return $this->fulfillment($response);
        }

        // if($action == 'nama_pelapor') {
        //     // get data user
        //     $customer   = Users::where('telp_no', $phone);
        //     $checker    = $customer->first();
        //     $name       = (isset($requestJson["queryResult"]["que? $requestJson["queryResult"]["queryText"] : null);
        //     // jika customer belum terdaftar
        //     if (is_null($checker)) {
        //         $customer->create([
        //             'name' => $name,
        //             'telp_no' => $phone,
        //             'is_active' => 1
        //         ]);
        //     } else {
        //         // jika customer sudah terdaftar
        //         $customer->update([
        //             'name' => $name
        //         ]);
        //     }
        // }

        // if($action == 'email_pelapor') {
        //     // get data user
        //     $customer   = Users::where('telp_no', $phone);
        //     $email       = (isset($requestJson["queryResult"]["queryText"]) ? $requestJson["queryResult"]["queryText"] : null);
            
        //     $customer->update([
        //         'email' => $email
        //     ]);
        // }

        // if($action == 'alamat') {
        //     // get data user
        //     $customer   = Users::where('telp_no', $phone);
        //     $alamat       = (isset($requestJson["queryResult"]["queryText"]) ? $requestJson["queryResult"]["queryText"] : null);
            
        //     TComplaint::where([
        //         ['m_user_fe_id',$customer->first()->id]
        //         ])->orderBy('created_at','DESC')->first()->update([
        //         'location' => $alamat
        //     ]);
        // }
        
        // if($action == 'fallback_isiData') {
        //     $tmp_customer  = TmpUsers::where('telp_no', $phone)->first();

        //     if($tmp_customer->tmp_variable == 1){
        //         $response   = "Masukkan dengan format nama, contoh: 'Tedi Setiawan'";
        //     }else if($tmp_customer->tmp_variable == 2){
        //         $response   = "Masukkan dengan format email, contoh: 'tedisetiawan@gmail.com'";
        //     }else if($tmp_customer->tmp_variable == 3){
        //         $response   = "Masukkan dengan format NIK, contoh: '3123456789987654'";
        //     }else if($tmp_customer->tmp_variable == 4){
        //         $response   = "Masukkan dengan format SIM, contoh: '3123456789987654'";
        //     }else if($tmp_customer->tmp_variable == 5){
        //         $response   = "Masukkan dengan format Kartu Mahasiswa, contoh: '3123456789987654'";
        //     }else if($tmp_customer->tmp_variable == 6){
        //         $response   = "Masukkan dengan format Kartu Pelajar, contoh: '3123456789987654'";
        //     }
            
        //     return $this->fulfillment($response);
        // }
        
        // if($action == 'notice_intent2') {
        //     $customer   = Users::where('telp_no', $phone)->first();
        //     // jika customer belum terdaftar
        //     if (is_null($customer)) {
        //         $response = "Anda belum terdaftar.\n";
        //         $response .= "Nama : \n";
        //         $response .= "Email : \n";
        //         $response .= "Tanggal Registrasi : \n";
        //         $response .= "NIK : \n";
        //         $response .= "Kartu Pelajar : \n";
        //         $response .= "Kartu Mahasiswa :\n";
        //         $response .= "Apakah Anda ingin mengubah data profil Anda?\n\n";
        //         $response .= "1. Ya\n";
        //         $response .= "2. Kembali\n";
        //     } else {
        //         $regis = Carbon::createFromFormat('Y-m-d H:i:s', $customer->created_at)->format('d-m-Y');
                
        //         $response = "Berikut data profil Anda.\n";
        //         $response .= "Nama : ". $customer->name ."\n";
        //         $response .= "Email : ". $customer->email ."\n";
        //         $response .= "Tanggal Registrasi : ". $regis ."\n";
        //         $response .= "NIK : ". $customer->NIK ."\n";
        //         $response .= "SIM : ". $customer->SIM ."\n";
        //         $response .= "Kartu Pelajar : ". $customer->KartuPelajar ."\n";
        //         $response .= "Kartu Mahasiswa : ". $customer->KartuMahasiswa ."\n\n";
        //         $response .= "Apakah Anda ingin mengubah data profil Anda?\n";
        //         $response .= "1. Ya\n";
        //         $response .= "2. Kembali";
        //     }

        //     Users::where('telp_no', $phone)->update([
        //         'session' => 'yk'
        //     ]);

        //     return $this->fulfillment($response);
        // }

        // if($action == 'send_master_complaint') {
        //     $m_complaint = \DB::table('m_complaints')->where([
        //     ['is_active', '1']
        //     ])->orderBy('id')->get();

        //     $response   = "Pilih Menu Komplain :\n";
        //     $id = 1;
        //     foreach($m_complaint as $complaint => $value){
        //         $response   .= $value->id . ". $value->name\n";    
        //         $id = $value->id;
        //     }
        //     $response   .= "18. Kembali\n";
        //     $response   .= "\nMasukkan hanya angkanya saja (1 - ". $id + 1 .")";

        //     $customer   = Users::where('telp_no', $phone)->first();
        //     $complaint   = TComplaint::where('m_user_fe_id', $customer->id)->where('tmp_value', 'CONFIRM')->orderBy('created_at','DESC')->first();
        //     if(!is_null($complaint)){
        //         $complaint->update([
        //             'tmp_value' => 'TYPE'
        //         ]);
        //     }

        //     Users::where('telp_no', $phone)->update([
        //         'session' => 'variable_type_complaint'
        //     ]);

        //     return $this->fulfillment($response);
        // }

        // if($action == 'master_complaint') {
        //     $id       = (isset($requestJson["queryResult"]["queryText"]) ? $requestJson["queryResult"]["queryText"] : null);

        //     $customer   = Users::where('telp_no', $phone);
        //     $complaint = \DB::table('t_complaint')->where([
        //         ['m_user_fe_id', '=', $customer->first()->id]
        //         ])->whereNull('tmp_variable')->orWhere('tmp_variable','<>','')->orderBy('id','DESC')->first();

        //     if(is_null($complaint)){
        //         $complaint_last = \DB::table('t_complaint')->where([
        //             ['complaint_no', 'LIKE', 'C%']
        //             ])->orderBy('id','DESC')->first();
                
        //         $complaint_no = ++$complaint_last->complaint_no;
    
        //         TComplaint::create([
        //             'm_complaint_id' => $id,
        //             'm_user_fe_id' => $customer->first()->id,
        //             'complaint_no' => $complaint_no,
        //             'is_active' => 1,
        //             'created_by' => $customer->first()->id,
        //             'updated_by' => $customer->first()->id
        //         ]);
    
        //         $m_complaint = \DB::table('m_complaints')->where([
        //             ['id', $id]
        //             ])->first();
    
        //         $response = "Baik, untuk jenis pengaduan, anda memilih " . $m_complaint->name;
        //         $response .= "\n\nSelanjutnya, pilih data yang akan diisi.\n";
        //     }else{
        //         if($complaint->tmp_value == "CONFIRM"){
        //             $response = "Pilih data yang akan diisi.\n";
        //         }else{
        //             TComplaint::where([
        //                 ['m_user_fe_id', '=', $customer->first()->id]
        //                 ])->whereNull('tmp_variable')->orWhere('tmp_variable','<>','')->orderBy('id','DESC')->first()->update([
        //                 'm_complaint_id' => $id
        //             ]);
                    
        //             $m_complaint = \DB::table('m_complaints')->where([
        //                 ['id', $id]
        //                 ])->first();
        
        //             $response = "Baik, untuk jenis pengaduan, anda memilih " . $m_complaint->name;
        //             $response .= "\n\nSelanjutnya, pilih data yang akan diisi.\n";
        //         }
        //     }
            
        //     $response .= "1. Nama Terlapor\n";
        //     $response .= "2. Tanggal Kejadian\n";
        //     $response .= "3. Lokasi Kejadian\n";
        //     $response .= "4. Deskripsi Pengaduan\n";
        //     $response .= "5. Bukti Pengaduan\n";
        //     $response .= "6. Clue Lain\n";
        //     $response .= "7. Kembali";

        //     Users::where('telp_no', $phone)->update([
        //         'session' => 'complaint'
        //     ]);

        //     return $this->fulfillment($response);
        // }

        // if($action == 'ConfirmDoneComplaint') {
        //     $customer   = Users::where('telp_no', $phone);
        //     $complaint   = TComplaint::where('m_user_fe_id', $customer->first()->id)->orderBy('created_at','DESC')->first();

        //     if(($complaint->tmp_variable) == 1){
        //         TComplaint::where('m_user_fe_id',$customer->first()->id)->orderBy('created_at','DESC')->first()->update([
        //             'nama_terlapor' => $complaint->tmp_value
        //         ]);
        //         // $response = "DONE\n";
        //     }else if(($complaint->tmp_variable) == 2){
        //         TComplaint::where('m_user_fe_id',$customer->first()->id)->orderBy('created_at','DESC')->first()->update([
        //             'date' => Carbon::createFromFormat('d/m/Y', $complaint->tmp_value)->format('Y-m-d')
        //         ]);
                
        //     }else if(($complaint->tmp_variable) == 3){
        //         TComplaint::where('m_user_fe_id',$customer->first()->id)->orderBy('created_at','DESC')->first()->update([
        //             'location' => $complaint->tmp_value
        //         ]);
        //     }else if(($complaint->tmp_variable) == 4){
        //         TComplaint::where('m_user_fe_id',$customer->first()->id)->orderBy('created_at','DESC')->first()->update([
        //             'description' => $complaint->tmp_value
        //         ]);
        //     }else if(($complaint->tmp_variable) == 6){
        //         TComplaint::where('m_user_fe_id',$customer->first()->id)->orderBy('created_at','DESC')->first()->update([
        //             'clue' => $complaint->tmp_value
        //         ]);
        //     }

        //     $m_complaint = \DB::table('m_complaints')->where([
        //         ['id', $complaint->m_complaint_id]
        //         ])->first();
            

        //     $complaint   = TComplaint::where('m_user_fe_id', $customer->first()->id)->orderBy('created_at','DESC')->first();
        //     if(!is_null($complaint->date)){
        //         $date = Carbon::createFromFormat('Y-m-d', $complaint->date)->format('d-m-Y');
        //     }else{
        //         $date = "-";
        //     }            
            
        //     $response = "Berikut data pengaduan Anda.\n";
        //     $response .= "Nama Terlapor : ". $complaint->nama_terlapor ."\n";
        //     $response .= "Jenis Pengaduan : ". $m_complaint->name ."\n";
        //     $response .= "Tanggal Kejadian : ". $date ."\n";
        //     $response .= "Lokasi Kejadian : ". $complaint->location ."\n";
        //     $response .= "Deskripsi : ". $complaint->description ."\n";
        //     $response .= "Clue Tambahan : ". $complaint->clue ."\n\n";
        //     $response .= "Apakah Anda sudah selesai mengisi data pengaduan?\n\n";
        //     $response .= "1. Ya\n";
        //     $response .= "2. Belum";

        //     $complaint->update([
        //         'tmp_value' => 'CONFIRM'
        //     ]);

        //     Users::where('telp_no', $phone)->update([
        //         'session' => 'yn'
        //     ]);

        //     return $this->fulfillment($response);
        // }

        if($action == 'Pengaduan_Submitted') {
            $customer   = Users::where('telp_no', $phone);
            $complaint_no   = TComplaint::where('m_user_fe_id', $customer->first()->id)->orderBy('id','DESC')->first()->complaint_no;

            // $complaint_last = \DB::table('t_complaint')->where([
            //     ['complaint_no', 'LIKE', 'C%']
            //     ])->orderBy('id','DESC')->first();
            
            // $complaint_no = ++$complaint_last->complaint_no;

            // TComplaint::create([
            //     'm_user_fe_id' => $tmp_complaint->m_user_fe_id,
            //     'complaint_no' => $complaint_no,
            //     'is_active' => 1,
            //     'created_by' => $customer->first()->id,
            //     'updated_by' => $customer->first()->id,
            //     'nama_terlapor' => $tmp_complaint->nama_terlapor,
            //     'date' => $tmp_complaint->date,
            //     'location' => $tmp_complaint->location,
            //     'description' => $tmp_complaint->description,
            //     'clue' => $tmp_complaint->clue
            // ]);

            
            TComplaint::where('complaint_no', $complaint_no)->first()->update([
                'is_active' => 1
                ]);
            $complaint = TComplaint::where('complaint_no', $complaint_no)->first();

            

            // if(!is_null($complaint->date)){
            //     $date = Carbon::createFromFormat('Y-m-d', $complaint->date)->format('d-m-Y');
            // }else{
            //     $date = "-";
            // }            
            
            $response = "Pengaduan Anda berhasil dibuat.\n\n";
            $response .= "Dengan detail sebagai berikut:\n";
            $response .= "Nomor Pengaduan : ". $complaint->complaint_no ."\n";
            $response .= "Nama Terlapor : ". $complaint->nama_terlapor ."\n";
            $response .= "Tanggal Kejadian : ". $complaint->original_date ."\n";
            $response .= "Lokasi Kejadian : ". $complaint->location ."\n";
            $response .= "Deskripsi : ". $complaint->description ."\n";
            $response .= "Clue Tambahan : ". $complaint->clue ."\n\n";
            $response .= "1. Menu\n";

            // TmpComplaint::where('m_user_fe_id', $customer->first()->id)->delete();

            // $complaint->update([
            //     'tmp_variable' => '',
            //     'tmp_value' => ''
            // ]);

            Users::where('telp_no', $phone)->update([
                'session' => 'menu_doang'
            ]);

            return $this->fulfillment($response);
        }

        if($action == 'input_no_pengaduan') {
            $complaint_no       = (isset($requestJson["queryResult"]["queryText"]) ? $requestJson["queryResult"]["queryText"] : null);
            $customer   = Users::where('telp_no', $phone);
            $complaint   = TComplaint::where('m_user_fe_id', $customer->first()->id)->where('complaint_no', $complaint_no)->orderBy('created_at','DESC')->first();

            if(!is_null($complaint)){
                if(!is_null($complaint->date)){
                    $date = Carbon::createFromFormat('Y-m-d', $complaint->date)->format('d-m-Y');
                }else{
                    $date = "-";
                }            
                if(!is_null($complaint->process_at)){
                    $date_process = Carbon::createFromFormat('Y-m-d', $complaint->process_at)->format('d-m-Y');
                }else{
                    $date_process = "-";
                }
                if(!is_null($complaint->clear_at)){
                    $date_finish = Carbon::createFromFormat('Y-m-d', $complaint->clear_at)->format('d-m-Y');
                }else{
                    $date_finish = "-";
                }
                
                $response = "Tracking Pengaduan : \n";
                $response .= $complaint->complaint_no ."\n";
                $response .= "- Pengaduan Diajukan : ". $date ."\n";
                $response .= "- Pengaduan Diproses : ". $date_process ."\n";
                $response .= "- Pengaduan Selesai : ". $date_finish ."\n\n";
                $response .= "1. Menu\n";
                $response .= "2. Tracking\n";    
            }else{
                $response = "Nomor aduan yang anda cari tidak ditemukan\n\n";
                $response .= "1. Menu\n";
                $response .= "2. Tracking\n";    
            }
            
            Users::where('telp_no', $phone)->update([
                'session' => 'track'
            ]);

            return $this->fulfillment($response);
        }

        if($action == 'Confirm_delete_complaint') {
            $complaint_no       = (isset($requestJson["queryResult"]["queryText"]) ? $requestJson["queryResult"]["queryText"] : null);
            $customer   = Users::where('telp_no', $phone);
            $complaint   = TComplaint::where('m_user_fe_id', $customer->first()->id)->where('complaint_no', $complaint_no)->orderBy('created_at','DESC')->first();

            if(!is_null($complaint)){                
                $response = "Berikut Data Pengaduan ". $complaint->complaint_no .".\n";
                $response .= "Nama Terlapor : ". $complaint->nama_terlapor ."\n";
                $response .= "Tanggal Kejadian : ". $complaint->original_date ."\n";
                $response .= "Lokasi Kejadian : ". $complaint->location ."\n";
                $response .= "Deskripsi : ". $complaint->description ."\n";
                $response .= "Clue Tambahan : ". $complaint->clue ."\n\n";
                $response .= "Apakah Anda yakin ingin menghapus pengaduan Anda?\n";
                $response .= "1. Ya\n";
                $response .= "2. Tidak\n";

            TComplaint::where([
                    ['m_user_fe_id',$customer->first()->id],
                    ['complaint_no',$complaint_no]
                    ])->orderBy('created_at','DESC')->first()->update([
                        'tmp_variable' => "delete"
                    ]);
  
            }else{
                $response = "Nomor aduan yang anda cari tidak ditemukan\n\n";
                $response .= "1. Menu\n";
                $response .= "2. Cari nomor pengaduan\n";    
            }

            Users::where('telp_no', $phone)->update([
                'session' => 'yb_np'
            ]);

            return $this->fulfillment($response);
        }
        
        if($action == 'delete_complaint') {
            $confirm        = (isset($requestJson["queryResult"]["queryText"]) ? $requestJson["queryResult"]["queryText"] : null);
            $customer       = Users::where('telp_no', $phone);

            if($confirm == 1){          
                $check = TComplaint::where([
                    ['m_user_fe_id',$customer->first()->id],
                    ['tmp_variable',"delete"]
                    ])->first();

                if(is_null($check)){
                    $response = "Anda membatalkan penghapusan complaint.\n\n1. Menu";
                }else{
                    TComplaint::where([
                        ['m_user_fe_id',$customer->first()->id],
                        ['tmp_variable',"delete"]
                        ])->delete();
                        
                    $response = "Data Complaint Anda berhasil dihapus.\n\n1. Menu";
                }
                
            }else if($confirm == 2){
                TComplaint::where([
                    ['m_user_fe_id',$customer->first()->id],
                    ['tmp_variable',"delete"]
                    ])->orderBy('created_at','DESC')->first()->update([
                        'tmp_variable' => ""
                    ]);
                    
                $response = "Data Anda tidak jadi dihapus.\n\n1. Menu";
            }
            Users::where('telp_no', $phone)->update([
                'session' => 'home'
            ]);

            return $this->fulfillment($response);
        }

        // if($action == 'deskripsi_pengaduan') {
        //     $deskripsi  = (isset($requestJson["queryResult"]["queryText"]) ? $requestJson["queryResult"]["queryText"] : null);
        //     $customer   = Users::where('telp_no', $phone);
            
        //     TComplaint::where([
        //             ['m_user_fe_id',$customer->first()->id]
        //         ])->orderBy('created_at','DESC')->first()->update([
        //         'description' => $deskripsi
        //     ]);
        // }

        // if($action == 'tgl') {
        //     // $date = Carbon::parse('02/10/2020 00:00:00');
        //     $date  = Carbon::parse(isset($requestJson["queryResult"]["queryText"]) ? $requestJson["queryResult"]["queryText"] : null);
        //     $customer   = Users::where('telp_no', $phone);
        //     TComplaint::where([
        //             ['m_user_fe_id',$customer->first()->id]
        //         ])->orderBy('created_at','DESC')->first()->update([
        //         'date' => $date
        //     ]);
        // }

        // if($action == 'send_link_foto') {
        //     $customer   = Users::where('telp_no', $phone);
        //     $TComplaint = TComplaint::where([
        //             ['m_user_fe_id',$customer->first()->id]
        //             ])->orderBy('created_at','DESC')->first();

        //     $response = "Baik, kami catat ya. Untuk kelancaran proses analisa pengaduan Anda. Mohon mengirimkan bukti pengaduan Anda berupa foto melalui link berikut.\n";
        //     $response .= "https://itjen-chatbot.herokuapp.com/upload/". $TComplaint->complaint_no;
        //     $response .= "\n\nAtau Anda bisa membatalkan pengaduan dengan ketik 'batal' ";
        //     return $this->fulfillment($response);
        // }

        // if($action == 'id_terlapor') {
        //     $nama_terlapor  = (isset($requestJson["queryResult"]["queryText"]) ? $requestJson["queryResult"]["queryText"] : null);
        //     $customer   = Users::where('telp_no', $phone);
        //     TComplaint::where([
        //             ['m_user_fe_id',$customer->first()->id]
        //         ])->orderBy('created_at','DESC')->first()->update([
        //         'nama_terlapor' => $nama_terlapor
        //     ]);
        // }

        // if($action == 'clue') {
        //     $clue  = (isset($requestJson["queryResult"]["queryText"]) ? $requestJson["queryResult"]["queryText"] : null);
        //     $customer   = Users::where('telp_no', $phone);
        //     TComplaint::where([
        //             ['m_user_fe_id',$customer->first()->id]
        //         ])->orderBy('created_at','DESC')->first()->update([
        //         'clue' => $clue
        //     ]);
        // }

        // if($action == 'ringkasan') {
        //     // $clue  = Carbon::parse(isset($requestJson["queryResult"]["queryText"]) ? $requestJson["queryResult"]["queryText"] : null);
        //     $customer   = Users::where('telp_no', $phone);
        //     $TComplaint=  TComplaint::where([
        //             ['m_user_fe_id',$customer->first()->id]
        //             ])->orderBy('created_at','DESC')->first();
            
        //     $date = Carbon::createFromFormat('Y-m-d', $TComplaint->date)->format('d-m-Y');

        //     $response = "Terima kasih. Berikut ringkasan dari pengaduan Anda dengan nomor pengaduan ". $TComplaint->complaint_no ;
        //     $response .= "\n1. Nama Terlapor : ". $TComplaint->nama_terlapor;
        //     $response .= "\n2. Pengaduan : ". $TComplaint->description;
        //     $response .= "\n3. Lokasi : ". $TComplaint->location;
        //     $response .= "\n4. Tanggal : ". $date;
        //     $response .= "\n5. Clue lain : ". $TComplaint->clue;
            
        //     $response .= "\n\nSilahkan cek secara berkala email anda ". $customer->first()->email ." dan posko pengaduan chatbot ini untuk kami informasikan kembali update mengenai pengaduan Anda. \n\nTerima kasih.";

        //     return $this->fulfillment($response);
        // }

        // if($action == 'info') {
        //     $response   = "Menu bantuan / informasi PESAT Chatbot\n";
        //     $response   .= "'profil' -> Informasi data profil anda\n";
        //     $response   .= "'ubah nama' -> Mengubah data nama anda \n";
        //     $response   .= "'ubah alamat' -> Mengubah data alamat anda \n";
        //     $response   .= "'pesan' -> Link katalog\n";
        //     $response   .= "'batalkan' -> Link untuk membatalkan pesanan anda\n";
        //     $response   .= "'cek' -> Link untuk melihat status pesanan anda\n";
        //     return $this->fulfillment($response);
        // }
    }

    public function upload($id)
    {
        $TComplaint = TComplaint::where('complaint_no', $id)->first();
        // dd($TComplaint->complaint_no);
        return view('upload', ['TComplaint' => $TComplaint]);
    }
    
    // method untuk upload bukti pengaduan
    public function uploadBukti(Request $request)
    {
        $complaint_no = request('complaint_no');
        $complaint = \DB::table('t_images');
        
        $TComplaint = \DB::table('t_complaint')->where('complaint_no', $complaint_no)->first();
        
        //dd($TComplaint);
        if(is_null($TComplaint)) {
            return redirect()->back()->with('message', 'Complaint No. yang kamu masukkan salah!');
        }

        $image = \DB::table('t_images')->where('description', $complaint_no);

        $pictureName = time(). '.' . $request->picture->getClientOriginalExtension();
        $request->picture->move(public_path('complaint'), $pictureName);
        
        $customer   = Users::where('id', $TComplaint->m_user_fe_id)->first();

        if (!is_null($image->first())){
            $picturePath = "complaint/" . $image->first()->name;
            if(\File::exists($picturePath)) {
                \File::delete($picturePath);
            }
            $image->update([
                'name' => $pictureName  
            ]);
        }else{
            \DB::table('t_images')->insert([
                'reference_id' => $TComplaint->id,
                'name' => $pictureName,
                'type' => 't_complaint',
                'path' => 'complaint',
                'created_by' => $customer->id,
                'updated_by' => $customer->id,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'description' => $TComplaint->complaint_no
            ]);    
        }

        
        // dd($customer->first());
        $from  = '+14155238886';
        $to    = $customer->telp_no;
        $body  = "Anda berhasil mengupload bukti pengaduan, apakah Anda ingin lanjut?\n";
        $body  .= "1. Ya \n";
        $body  .= "2. Tidak \n";
        $twilio = new Twilio;
        $twilio->sendWhatsAppSMS($from, $to, $body);
        // TComplaint::where('complaint_no', $complaint_no)->update([
        //     'id_status' => 2
        // ]);

        // event(new ConfirmPayment($TComplaint->complaint_no));

        return view('upload-success');
    }

    public function fulfillment($response)
    {
        $fulfillment = array(
            "fulfillmentText" => $response
         );
    
        echo(json_encode($fulfillment));
    }

    public function fulfillment_withContext($response, $context_name)
    {
        $context = array();
        $context[0]['lifespanCount'] = 2;
        $context[0]['name'] = $context_name;

        $fulfillment = array(
            'fulfillmentText' => $response,
            'outputContexts' => $context,
         );
    
        echo(json_encode($fulfillment));
    }
}
