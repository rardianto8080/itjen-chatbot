<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TmpUsers extends Model
{
    // define table
    protected $table = 'tmp_users';
    protected $primaryKey = 'id';

    protected $guarded = [];

}
