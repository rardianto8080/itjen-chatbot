<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TComplaint extends Model
{
    // define table
    protected $table = 't_complaint';
    protected $primaryKey = 'id';

    protected $guarded = [];

}
