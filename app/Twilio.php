<?php
 
namespace App;
// Use the REST API Client to make requests to the Twilio REST API
use Twilio\Rest\Client;
 
class Twilio {
 
    private $sid = "ACfb39a5f03e65f5069e33af2d6194cc6c"; // Your Account SID from www.twilio.com/console
    private $token = "b72b5666dc05567bd6dc5d06f87ee9fc"; // Your Auth Token from www.twilio.com/console
 
    private $client;
 
    public function __construct() {
        $this->client = new Client($this->sid, $this->token);
    }
 
    public function sendSMS($from, $body, $to) {
        $message = $this->client->messages->create(
            $to, // Text this number
            array(
              'from' => $from, // From a valid Twilio number
              'body' => $body
            )
        );
        return $message->sid;
    }

    public function sendWhatsAppSMS($from, $to, $body) {
        $message = $this->client->messages
                  ->create("whatsapp:" . $to, // to
                           array(
                               "from" => "whatsapp:" . $from,
                               "body" => $body
                           )
                  );
        return $message;          
    }
}